function calculateTicketCost() {
    const ticketNumber = document.getElementById('ticketNumber').value;
    const customerName = document.getElementById('customerName').value;
    const destination = document.getElementById('destination').value;
    const tripType = parseInt(document.getElementById('tripType').value);
    const price = parseFloat(document.getElementById('price').value);

    let basePrice = price;
    if (tripType === 2) {
        basePrice = price * 1.8;
    }

    const subtotal = basePrice;
    const tax = subtotal * 0.16;
    const total = subtotal + tax;

    document.getElementById('displayTicketNumber').textContent = ticketNumber;
    document.getElementById('displayCustomerName').textContent = customerName;
    document.getElementById('displayDestination').textContent = destination;
    document.getElementById('displayTripType').textContent = tripType === 1 ? 'Sencillo' : 'Doble';
    document.getElementById('displayPrice').textContent = price.toFixed(2);
    document.getElementById('displaySubtotal').textContent = subtotal.toFixed(2);
    document.getElementById('displayTax').textContent = tax.toFixed(2);
    document.getElementById('displayTotal').textContent = total.toFixed(2);

    // Muestra la sección de resultados
    document.querySelector('.result-section').style.display = 'block';
}

function clearForm() {
    // Limpia los campos del formulario y oculta la sección de resultados
    document.getElementById('ticketForm').reset();
    document.querySelector('.result-section').style.display = 'none';
}

function sendData() {
    // Aquí puedes agregar el código para enviar los datos a un servidor, si es necesario
    // En este ejemplo, simplemente mostramos un mensaje de alerta
    alert('Datos enviados con éxito');
}